
var btnUse = document.querySelectorAll('.btn-use');


btnUse.forEach(function(btn) {
	btn.addEventListener("click", toggleActive);
});

// toggle mobile menu
var mobMenuToggle = document.getElementsByClassName('header__menu-toggle')[0];
var scrollLinks = document.getElementsByClassName('scroll-link');
var numScrollLinks = scrollLinks.length;

mobMenuToggle.addEventListener('click', function () {
    toggleMobileMenu(false);
});

function toggleMobileMenu(close) {
    if(!close) {
        mobMenuToggle.classList.toggle("open");
    } else {
        mobMenuToggle.classList.remove("open");
    }
}


// all anchor links
var lnks = document .querySelectorAll('.scroll-link');

for (var i = 0; i < lnks.length; i++) {
    lnks[i].onclick = function(e){
        e.preventDefault();
        var curLnk = this;

        var curLnkHash = curLnk.getAttribute("href").substring(1);
        console.log(curLnkHash);
        var targSec = document.getElementById(curLnkHash);

        if(!isHidden(mobMenuToggle)) {
            toggleMobileMenu(true);
        }

        window.scrollTo({
            top: targSec.offsetTop,
            behavior: "smooth"
        });

    };
}



// COOKIE ALERT

// COOKIE TOOLS
// END COOKIE TOOLS
function checkCookieAlert() {
    const isAgreed = localStorage.getItem('cookie_agreed');
    if ( !isAgreed ) {
        document.querySelector('.sec-cookie').style.display = null;
    }
}
function hideCookieAlert() {
    document.querySelector('.sec-cookie').style.display = 'none';
    localStorage.setItem('cookie_agreed', true);
}
document.querySelector('#cookie_agree').onclick = hideCookieAlert;
checkCookieAlert();
// END COOKIE ALERT